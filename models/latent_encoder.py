import yaml 
import sys 
import torch 
import os
from omegaconf import OmegaConf
from taming.models.vqgan import VQModel




class Encoder(torch.nn.Module):
    def __init__(self,configpath,ckpt_path,device):
        super(Encoder,self).__init__()
        self.device = device
        #configpath = "logs/vqgan_imagenet_f16_16384/configs/model.yaml"
        #ckpt_path = "logs/vqgan_imagenet_f16_16384/checkpoints/last.ckpt"
        config16384 = self.load_config(configpath, display=False)
        model16384 = self.load_vqgan(config16384, ckpt_path=ckpt_path).to(device)

        self.net = model16384
        
    def forward(self,x):
        with torch.no_grad():
            z, _, [_, _, indices] = self.net.encode(x)
        return z
    def decoding(self,z):
         with torch.no_grad():
            xbar = self.net.decode(z)
            return xbar
        
    def load_config(self,config_path, display=False):
        config = OmegaConf.load(config_path)
        if display:
            print(yaml.dump(OmegaConf.to_container(config)))
        return config

    def load_vqgan(self,config, ckpt_path=None, is_gumbel=False):
        if is_gumbel:
            model = GumbelVQ(**config.model.params)
        else:
            model = VQModel(**config.model.params)
        if ckpt_path is not None:
            sd = torch.load(ckpt_path, map_location="cpu")["state_dict"]
            missing, unexpected = model.load_state_dict(sd, strict=False)
        return model.eval()