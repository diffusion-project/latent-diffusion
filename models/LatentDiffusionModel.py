import torch 
from torch import nn 
import torch.nn.functional as F
import math

class LDM(nn.Module):
    
    def __init__(self,
                 vq_model,
                 net=None,
                 diffusion_steps = 50, 
                 out_shape = (3,128,128),
                 conditional_shape = (3,128,128), 
                 noise_schedule = 'linear', 
                 beta_1 = 1e-4, 
                 beta_T = 0.02,
                 alpha_bar_lower_bound = 0.9,
                 var_schedule = 'same', 
                 kl_loss = 'simplified', 
                 recon_loss = 'none',
                 device=None):
        '''
        net:                   UNet
        diffusion_steps:       Length of the Markov chain
        out_shape:             Shape of the UNet's models's in- and output images
        conditional_shape:     Shape of the low resolution image the DM is conditioned on for Super Resolution
        noise_schedule:        Methods of initialization for the noise dist. variances, 'linear', 'cosine' or bounded_cosine
        beta_1, beta_T:        Variances for the first and last noise dist. (only for the 'linear' noise schedule)
        alpha_bar_lower_bound: Upper bound for the varaince of the complete noise dist. (only for the 'cosine_bounded' noise schedule)
        var_schedule:          Options to initialize or learn the denoising dist. variances, 'same', 'true'
        kl_loss:               Choice between the mathematically correct 'weighted' or in practice most commonly used 'simplified' KL loss
        recon_loss:            Is 'none' to ignore the reconstruction loss or 'nll' to compute the negative log likelihood
        '''
        super(LDM,self).__init__()
        self.device = device
        # set the vqgan (encodes input when training and decodes the input when sampling)
        self.vq_model = vq_model.to(device)
        # initialize the beta's, alpha's and alpha_bar's for the given noise schedule
        if noise_schedule == 'linear':
            beta, alpha, alpha_bar = self.linear_schedule(diffusion_steps, beta_1, beta_T, device=self.device)
        elif noise_schedule == 'cosine':
            beta, alpha, alpha_bar = self.cosine_schedule(diffusion_steps, device=self.device)
        elif noise_schedule == 'cosine_bounded':
            beta, alpha, alpha_bar = self.bounded_cosine_schedule(diffusion_steps, alpha_bar_lower_bound, device=self.device)
        else:
            raise ValueError('Unimplemented noise scheduler')
            
        # initialize the denoising varainces for the given varaince schedule 
        if var_schedule == 'same':
            var = beta
        elif var_schedule == 'true':
            var = [beta[0]] + [((1-alpha_bar[t-1])/(1-alpha_bar[t]))*beta[t] for t in range (1,diffusion_steps)]
            var = torch.tensor(var, device=self.device)
        else:
            raise ValueError('Unimplemented variance scheduler')
        
        # check for invalid kl_loss argument
        if (kl_loss != 'simplified') & (kl_loss != 'weighted'): 
            raise ValueError("Unimplemented loss function")
        
        self.net = net
        self.diffusion_steps = diffusion_steps
        self.noise_schedule = noise_schedule
        self.var_schedule = var_schedule
        self.beta = beta
        self.alpha = alpha
        self.alpha_bar = alpha_bar
        self.sqrt_1_minus_alpha_bar = torch.sqrt(1-alpha_bar) # for forward std
        self.sqrt_alpha_bar = torch.sqrt(alpha_bar) # for forward mean
        self.var = var
        self.std = torch.sqrt(self.var)
        self.kl_loss = kl_loss 
        self.recon_loss = recon_loss 
        self.out_shape = out_shape
        self.conditional_shape = conditional_shape
        # precomputed for efficiency reasons
        self.noise_scaler = (1-alpha)/( self.sqrt_1_minus_alpha_bar)
        self.mean_scaler = 1/torch.sqrt(self.alpha)
        self.mse_weight = (self.beta**2)/(2*self.var*self.alpha*(1-self.alpha_bar))
          
    @staticmethod
    def linear_schedule(diffusion_steps, beta_1, beta_T, device):
        ''''
        Function that returns the noise distribution hyperparameters for the linear schedule.  

        Parameters:
        diffusion_steps (int): Length of the Markov chain.
        beta_1        (float): Variance of the first noise distribution.
        beta_T        (float): Variance of the last noise distribution.
        
        Returns:
        beta      (tensor): Linearly scaled from beta[0] = beta_1 to beta[-1] = beta_T, length is diffusion_steps.
        alpha     (tensor): Length is diffusion_steps.
        alpha_bar (tensor): Length is diffusion_steps.
        '''
        beta = torch.linspace(beta_1, beta_T, diffusion_steps,device=device)
        alpha = 1 - beta
        alpha_bar = torch.cumprod(alpha, dim=0)
        return beta, alpha, alpha_bar
    
    @staticmethod    
    def cosine_schedule(diffusion_steps, device):
        '''
        Function that returns the noise distribution hyperparameters for the cosine schedule.
        From "Improved Denoising Diffusion Probabilistic Models" by Nichol and Dhariwal.

        Parameters:
        diffusion_steps (int): Length of the Markov chain.
        
        Returns:
        beta      (tensor): Length is diffusion_steps.
        alpha     (tensor): Length is diffusion_steps.
        alpha_bar (tensor): Follows a sigmoid-like curve with a linear drop-off in the middle. 
                            Length is diffusion_steps.
        '''
        cosine_0 = LDM.cosine(torch.tensor(0, device=device), diffusion_steps= diffusion_steps)
        alpha_bar = [LDM.cosine(torch.tensor(t, device=device),diffusion_steps = diffusion_steps)/cosine_0 
                     for t in range(1, diffusion_steps+1)]
        shift = [1] + alpha_bar[:-1]
        beta = 1 - torch.div(torch.tensor(alpha_bar, device=device), torch.tensor(shift, device=device))
        beta = torch.clamp(beta, min =0, max = 0.999).to(device) #suggested by paper
        alpha = 1 - beta
        alpha_bar = torch.tensor(alpha_bar,device=device)
        return beta, alpha, alpha_bar
    
    @staticmethod    
    def bounded_cosine_schedule(diffusion_steps, alpha_bar_lower_bound, device):
        '''
        Function that returns the noise distribution hyperparameters for our experimental version of a 
        bounded cosine schedule. Benefits are still unproven. It still has a linear drop-off in alpha_bar, 
        but it's not sigmoidal and the betas are no longer smooth.

        Parameters:
        diffusion_steps (int): Length of the Markov chain
        
        Returns:
        beta      (tensor): Length is diffusion_steps
        alpha     (tensor): Length is diffusion_steps
        alpha_bar (tensor): Bounded between (alpha_bar_lower_bound, 1) with a linear drop-off in the middle. 
                            Length is diffusion_steps
        '''
        # get cosine alpha_bar (that range from 1 to 0)
        _, _, alpha_bar = LDM.cosine_schedule(diffusion_steps, device)
        # apply min max normalization on alpha_bar (range from lower_bound to 0.999)
        min_val = torch.min(alpha_bar)
        max_val = torch.max(alpha_bar)
        alpha_bar = (alpha_bar - min_val) / (max_val - min_val)
        alpha_bar = alpha_bar * (0.9999 - alpha_bar_lower_bound) + alpha_bar_lower_bound # for 0.9999=>beta_1 = 1e-4
        # recompute beta, alpha and alpha_bar
        alpha_bar = alpha_bar.tolist()
        shift = [1] + alpha_bar[:-1]
        beta = 1 - torch.div(torch.tensor(alpha_bar, device = device), torch.tensor(shift, device=device))
        beta = torch.clamp(beta, min=0, max = 0.999)
        beta = torch.tensor(sorted(beta), device = device)
        alpha = 1 - beta
        alpha_bar = torch.cumprod(alpha, dim=0)
        return beta, alpha, alpha_bar

    @staticmethod
    def cosine(t, diffusion_steps, s = 0.008):
        '''
        Helper function that computes the cosine function from "Improved Denoising Diffusion Probabilistic Models" 
        by Nichol and Dhariwal, used for the cosine noise schedules.

        Parameters:
        t               (int): Current timestep
        diffusion_steps (int): Length of the Markov chain
        s             (float): Offset value suggested by the paper. Should be chosen such that sqrt(beta[0]) ~ 1/127.5 
                               (for small T=50, this is not possible)
                                
        Returns:
        (numpy.float64): Value of the cosine function at timestep t
        '''
        return (torch.cos((((t/diffusion_steps)+s)*math.pi)/((1+s)*2)))**2
    
    
    ####
    # Important to note: Timesteps are adjusted to the range t in [1, diffusion_steps] akin to the paper 
    # equations, where x_0 denotes the input image, z_0 its encoding through the VQGAN and z_t the noised 
    # latent after adding noise t times on z_0.
    # Both trajectories are applied on batches assuming shape=(batch_size, channels, height, width).
    ####
    
    # Forward Trajectory Functions:
    
    @torch.no_grad()
    def forward_trajectory(self, z_0, t = None):
        '''
        Applies noise t times to each latent encoding in the batch z_0.
        
        Parameters:
        z_0 (tensor): Batch of latent encodings
        t   (tensor): Batch of timesteps, by default goes through full forward trajectory
    
        Returns:
        z_T           (tensor): Batch of noised latents at timestep t
        forward_noise (tensor): Batch of noise parameters from the noise distribution reparametrization used to draw z_T
        '''
        if t is None:
            t = torch.full((z_0.shape[0],), self.diffusion_steps, device = self.device)
        elif torch.any(t == 0):
            raise ValueError("The tensor 't' contains a timestep zero.")
        forward_noise = torch.randn(z_0.shape, device = self.device)
        z_T = self.noised_latent(forward_noise, z_0, t) 
        return z_T , forward_noise
    
    @torch.no_grad()
    def noised_latent(self, forward_noise, z_0, t):
        '''
        Given a batch of noise parameters, this function recomputes the batch of noised latents at their respective timesteps t.
        This allows us to avoid storing all the intermediate latents z_t along the forward trajectory.
        
        Parameters:
        forward_noise (tensor): Batch of noise parameters from the noise distribution reparametrization used to draw z_t
        z_0           (tensor): Batch of input latent encodings
        t             (tensor): Batch of timesteps
    
        Returns:
        x_t           (tensor): Batch of noised images at timestep t
        '''
        mean, std = self.forward_dist_param(z_0, t)
        z_t = mean + std*forward_noise
        return z_t
    
    @torch.no_grad()
    def forward_dist_param(self, z_0, t):
        '''
        Computes the parameters of the complete noise distribution.
        
        Parameters:
        z_0  (tensor): Batch of input latents
        t    (tensor): Batch of timesteps
    
        Returns:
        mean (tensor): Batch of means for the complete noise distribution for each image encoding in the batch z_0
        std  (tensor): Batch of std scalars for the complete noise distribution for each image encoding in the batch z_0
        '''
        mean = self.sqrt_alpha_bar[t-1][:,None,None,None]*z_0
        std = self.sqrt_1_minus_alpha_bar[t-1][:,None,None,None]
        return mean, std
    
    @torch.no_grad()
    def single_forward_dist_param(self, z_t_1, t):
        '''
        Computes the parameters of the individual noise distribution.

        Parameters:
        z_t_1 (tensor): Batch of noised image encodings at timestep t-1
        t     (tensor): Batch of timesteps
        
        Returns:
        mean (tensor): Batch of means for the individual noise distribution for each image in the batch z_t_1
        std  (tensor): Batch of std scalars for the individual noise distribution for each image in the batch z_t_1
        '''
        mean = torch.sqrt(1-self.beta[t-1])[:,None,None,None]*z_t_1
        std = torch.sqrt(self.beta[t-1])[:,None,None,None]
        return mean, std


    # Reverse Trajectory Functions:

    def reverse_dist_param(self, z_t, t, y):
        '''
        Computes the parameters of the reverse dist.

        Parameters:
        z_t (tensor): Batch of input latent encodings
        t   (tensor): Batch of timestep
        y (tensor):   Batch of conditional information for each encoding in the batch z_t
        
        Returns:
        z_t_1 (tensor): Batch of denoised image encodings at timestep t-1
        '''
        pred_noise = self.net(z_t,t,y)
        mean = self.mean_scaler[t-1][:,None,None,None]*(z_t - self.noise_scaler[t-1][:,None,None,None]*pred_noise)
        std = self.std[t-1][:,None,None,None]
        return mean, std
   
 
    # Forward Function (primarily for training)
 
    def forward(self, x_0, t, y):
        '''
        Encodes the input image x_0 with the model's VQGAN, applies noise to it t times, and computes the
        parameters of its denoising distribution with the use of the UNet. Given the noised latent, timestep, and
        conditioning information y, the UNet returns the predicted noise which is used to parameterize the mean and
        std of the denoising Gaussian. 
        Since the LDM class is inheriting from the nn.Module class, and this is the forward pass used in training, 
        this function is required to share the name 'forward', even though we are effectively applying one reverse 
        step in the trajectory. 

        Parameters:
        x_0 (tensor): Batch of input images, with color channels assumed to be normalized between [-1,1]
        t   (tensor): Batch of timesteps
        y (tensor):   Batch of conditional information for each input image
        
        Returns:
        mean        (tensor): Batch of means for the complete noise dist. for each latent encoding in the batch z_t
        std         (tensor): Batch of std scalars for the complete noise dist. for each latent encoding in the batch z_t
        pred_noise  (tensor): Predicted noise for each latent encoding in the batch z_t
        '''
        with torch.no_grad():
            z_0 = self.vq_model(x_0.to(self.device)).to(self.device)
            z_t, forward_noise = self.forward_trajectory(z_0,t)
        pred_noise = self.net(z_t,t,y)
        mean = self.mean_scaler[t-1][:,None,None,None]*(z_t - self.noise_scaler[t-1][:,None,None,None]*pred_noise)
        std = self.std[t-1][:,None,None,None]
        return mean, std, pred_noise,forward_noise 
    
    # Forward and Reverse Trajectory:

    @torch.no_grad()
    def complete_trajectory(self, x_0, y):
        '''
        Takes a batch of images, encodes them through the models VQGAN, and applies both trajectories sequentially, 
        i.e. first adds noise to all latent encodings along the forward chain and later removes the noise with the reverse chain.
        This function will be used in the evaluation pipeline as a means to evaluate its performance on 
        how well it is able to reconstruct/recover the training images after applying the forward trajectory.

        Parameters:
        x_0 (tensor): Batch of input images, with color channels assumed to be normalized between [-1,1]
        y (tensor):   Batch of conditional information for each input image
        
        Returns:
        x_0_recon (tensor): Batch of images given by the model reconstruction of x_0
        '''
        if self.vq_model.coder == "encoder":
            # apply encoder on the images to get latent encodings
            z_0 = self.vq_model(x_0.to(self.device)).to(self.device)
        else:
            z_0=x_0
        # apply forward trajectory
        z_0_recon, _ = self.forward_trajectory(z_0)
        # apply reverse trajectory
        for t in reversed(range(1, self.diffusion_steps + 1)):
            # draw noise used in the denoising dist. reparametrization
            if t > 1:
                noise = torch.randn(z_0_recon.shape, device=self.device)
            else:
                noise = torch.zeros(z_0_recon.shape, device=self.device)
            # get denoising dist. param
            mean, std = self.reverse_dist_param(z_0_recon, torch.full((z_0_recon.shape[0],), t, device = self.device), y)
            # compute the drawn denoised latent at time t
            z_0_recon = mean + std * noise
        if self.vq_model.coder == "decoder":
            # apply decoder on the reconstructed latent encoding to get original image back
            return self.vq_model(z_0_recon.to(self.device)).to(self.device)
        return z_0_recon 
      

    # Sampling Functions:

    @torch.no_grad()
    def sample(self, y, batch_size = 10,  z_T=None):

        '''
        Samples batch_size images by passing a batch of randomly drawn noise parameters through the complete 
        reverse trajectory. The generated batch of latent encodings are finally decoded into the images with the model's VQGAN. 
        The last denoising step is deterministic as suggested by the paper "Denoising Diffusion Probabilistic Models" by Ho et al.

        Parameters:
        y (tensor):       Batch of conditional information for each input image
        batch_size (int): Number of images to be sampled/generated from the diffusion model 
        x_T     (tensor): Input of the reverse trajectory. Batch of noised images usually drawn 
                          from an isotropic Gaussian, but can be set manually if desired.

        Returns:
        x_0 (tensor): Batch of sampled/generated images
        '''
        # start with a batch of isotropic noise images (or given arguemnt)
        if z_T:
            z_t_1 = z_T
        else:
            z_t_1 = torch.randn((batch_size,)+tuple(self.out_shape), device=self.device)
        # apply reverse trajectory
        for t in reversed(range(1, self.diffusion_steps+1)):
            # draw noise used in the denoising dist. reparametrization
            if t>1:
                noise = torch.randn(z_t_1.shape, device=self.device)
            else:
                noise = torch.zeros(z_t_1.shape, device=self.device)
            # get denoising dist. param
            mean, std = self.reverse_dist_param(z_t_1, torch.full((z_t_1.shape[0],), t ,device = self.device), y)
            # compute the drawn densoined latent at time t
            z_t_1 = mean + std*noise
        # quantize and decode the generated latent encoding
        x_0 = self.vq_model.decode(z_t_1.to(self.device)).to(self.device)
        return x_0

    @torch.no_grad()
    def sample_intermediates_latents(self, y):
        '''
        Samples a single latent encoding and provides all intermediate denoised images that were drawn along the reverse 
        trajectory. The generated abtch of encodings is finally decoded into a batch of images with the model's VQGAN.
        The last denoising step is deterministic as suggested by the paper "Denoising Diffusion Probabilistic Models" by Ho et al.
        
        Parameters:
        y (tensor): Batch of conditional information for each input image

        Returns:
        x (tensor): Contains the self.diffusion_steps+1 denoised image tensors
        '''
        # start with an image of pure noise (batch_size 1) and store it as part of the output 
        z_t_1 = torch.randn((1,) + tuple(self.out_shape), device=self.device)
        z = torch.empty((self.diffusion_steps+1,) + tuple(self.out_shape), device=self.device)
        z[-1] = z_t_1.squeeze(0)
        # apply reverse trajectory
        for t in reversed(range(1, self.diffusion_steps+1)):
            # draw noise used in the denoising dist. reparametrization
            if t>1:
                noise = torch.randn(z_t_1.shape, device=self.device)
            else:
                noise = torch.zeros(z_t_1.shape, device=self.device)
            # get denoising dist. param
            mean, std = self.reverse_dist_param(z_t_1, torch.full((z_t_1.shape[0],), t ,device = self.device), y)
            # compute the drawn densoined latent at time t
            z_t_1 = mean + std*noise
            # store noised image
            z[t-1] = z_t_1.squeeze(0)
        # quantize and decode the generated and all intermediate latent encodings
        x = self.vq_model(z.to(self.device)).to(self.device)
        return x


    # Loss functions
    
    def loss_simplified(self, forward_noise, pred_noise, t=None):
        '''
        Returns the Mean Squared Error (MSE) between the forward_noise used to compute the noised latent images z_t 
        along the forward trajectory and the predicted noise computed by the U-Net with the noised latents z_t and timestep t.
        '''
        return F.mse_loss(forward_noise, pred_noise)
    
    
    def loss_weighted(self, forward_noise, pred_noise, t):
        '''
        Returns the mathematically correct weighted version of the simplified loss.
        '''
        return self.mse_weight[t-1][:,None,None,None]*F.mse_loss(forward_noise, pred_noise)
    
    
    # If t=0 and self.recon_loss == 'nll'
    def loss_recon(self, z_0, mean_1, std_1):
        '''
        Returns the reconstruction loss given by the mean negative log-likelihood of z_0 under the last 
        denoising Gaussian distribution with mean mean_1 and standard deviation std_1.
        '''
        return -torch.distributions.Normal(mean_1, std_1).log_prob(z_0).mean()



