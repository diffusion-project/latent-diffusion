import yaml 
import sys 
import torch 
import os
from omegaconf import OmegaConf
from taming.models.vqgan import VQModel
from torch import nn



class VQGAN(torch.nn.Module):
    def __init__(self,configpath,ckpt_path,device, coder):
        super(VQGAN,self).__init__()
        #configpath = "logs/vqgan_imagenet_f16_16384/configs/model.yaml"
        #ckpt_path = "logs/vqgan_imagenet_f16_16384/checkpoints/last.ckpt"
        config = self.load_config(configpath, display=False)
        model = self.load_vqgan(config, ckpt_path=ckpt_path)
        self.device = device
        self.coder = coder
        if coder == "encoder":
            encoder = model.encoder
            quant_conv = model.quant_conv
            encode = nn.Sequential(encoder, quant_conv)
            self.encode = encode
        elif coder == "decoder":
            self.quantize = model.quantize
            self.post_quant_conv = model.post_quant_conv
            self.decoder = model.decoder
            #code = nn.Sequential(quantize, post_quant_conv, decoder)
            
        del model
        
    def forward(self,x):
        with torch.no_grad():
            return self.encode(x)
    def decode(self,x):
        d,_,_ = self.quantize(x)
        d = self.post_quant_conv(d)
        return self.decoder(d)

    def load_config(self,config_path, display=False):
        config = OmegaConf.load(config_path)
        if display:
            print(yaml.dump(OmegaConf.to_container(config)))
        return config

    def load_vqgan(self,config, ckpt_path=None, is_gumbel=False):
        if is_gumbel:
            model = GumbelVQ(**config.model.params)
        else:
            model = VQModel(**config.model.params)
        if ckpt_path is not None:
            sd = torch.load(ckpt_path, map_location="cpu")["state_dict"]
            missing, unexpected = model.load_state_dict(sd, strict=False)
        return model.eval()
