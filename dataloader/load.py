import torch
from torch.utils.data import Dataset
from torchvision import transforms
import os 
from PIL import Image
import pandas as pd
import numpy as np

class LatentDataset(Dataset):
    def __init__(self,fpath,img_size,train,frac =0.8,skip_first_n = 0,ext = ".png",transform=True,resolution=1024 ):
        """
        Args:
            fpath (string): Path to the folder where images are stored
            img_size (int): size of output image img_size=height=width
            ext (string): type of images used(eg .png)
            transform (Bool): Image augmentation for diffusion model
            skip_first_n: skips the first n values. Usefull for datasets that are sorted by increasing Likeliehood 
            train (Bool): Choose dataset to be either train set or test set. frac(float) required 
            frac (float): value within (0,1] (seeded)random shuffles dataset, then divides into train and test set. 
                            """
        
        ### Create DataFrame
        file_list = []
        for root, dirs, files in os.walk(fpath, topdown=False):
            for name in sorted(files):
                file_list.append(os.path.join(root, name))

        df = pd.DataFrame({"Filepath":file_list},)
        self.df = df[df["Filepath"].str.endswith(ext)] 
        
        
            
        if skip_first_n:
            self.df = self.df[skip_first_n:]
        
        if train: 
            df_train = self.df.sample(frac=frac,random_state=2)
            self.df = df_train
        else:
            df_train = self.df.sample(frac=frac,random_state=2)
            df_test = df.drop(df_train.index)
            self.df = df_test
            
        if resolution == 1024:
            if transform: 
                self.transform_down =  transforms.Resize(img_size,antialias=True)

                intermediate_size = 1100
                theta = np.pi/4 -np.arccos(intermediate_size/(np.sqrt(2)*1024)) #Check dataloading.ipynb in analysis-depot for more details

                self.transform_enc =  transforms.Compose([transforms.ToTensor(),
                                                          transforms.Resize(intermediate_size,antialias=True),
                                                          transforms.RandomRotation(theta/np.pi*180,interpolation=transforms.InterpolationMode.BILINEAR),
                                                          transforms.CenterCrop(1024),transforms.RandomHorizontalFlip(p=0.5)])
                self.transform_norm =transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))
            else : 
                ... #TODO
                
        elif resolution ==512:
            if transform:
                self.transform_down =  transforms.Resize(img_size,antialias=True)

                intermediate_size = 600
                theta = np.pi/4 -np.arccos(intermediate_size/(np.sqrt(2)*512)) #Check dataloading.ipynb in analysis-depot for more details

                self.transform_enc =  transforms.Compose([transforms.ToTensor(),
                                                          transforms.Resize(intermediate_size,antialias=True),
                                                          transforms.RandomRotation(theta/np.pi*180,interpolation=transforms.InterpolationMode.BILINEAR),
                                                          transforms.CenterCrop(512),transforms.RandomHorizontalFlip(p=0.5)])
                self.transform_norm =transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))
            else:
                ... # TODO
            
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self,idx):
        path =  self.df.iloc[idx].Filepath
        img = Image.open(path)
        img_enc = self.transform_enc(img).type(torch.float32)
        img_down = self.transform_down(img_enc)
        img_enc  = self.transform_norm(img_enc)
        img_down = self.transform_norm(img_down)
        
        return img_enc,img_down


    def tensor2PIL(self,img):
        back2pil = transforms.Compose([transforms.Normalize(mean=(-1,-1,-1),std=(2,2,2)),transforms.ToPILImage()])
        return back2pil(img)





class LatentDataset_uncon(Dataset):
    def __init__(self,fpath,img_size,train,frac =0.8,skip_first_n = 0,ext = ".png",transform=True,resolution=1024 ):
        """
        Args:
            fpath (string): Path to the folder where images are stored
            img_size (int): size of output image img_size=height=width
            ext (string): type of images used(eg .png)
            transform (Bool): Image augmentation for diffusion model
            skip_first_n: skips the first n values. Usefull for datasets that are sorted by increasing Likeliehood 
            train (Bool): Choose dataset to be either train set or test set. frac(float) required 
            frac (float): value within (0,1] (seeded)random shuffles dataset, then divides into train and test set. 
                            """

        ### Create DataFrame
        file_list = []
        for root, dirs, files in os.walk(fpath, topdown=False):
            for name in sorted(files):
                file_list.append(os.path.join(root, name))

        df = pd.DataFrame({"Filepath":file_list},)
        self.df = df[df["Filepath"].str.endswith(ext)]



        if skip_first_n:
            self.df = self.df[skip_first_n:]

        if train:
            df_train = self.df.sample(frac=frac,random_state=2)
            self.df = df_train
        else:
            df_train = self.df.sample(frac=frac,random_state=2)
            df_test = df.drop(df_train.index)
            self.df = df_test

        if resolution == 1024:
            if transform:
                self.transform_down =  transforms.Resize(img_size,antialias=True)

                intermediate_size = 1100
                theta = np.pi/4 -np.arccos(intermediate_size/(np.sqrt(2)*1024)) #Check dataloading.ipynb in analysis-depot for more details

                self.transform_enc =  transforms.Compose([transforms.ToTensor(),
                                        transforms.Resize(intermediate_size,antialias=True),
                                        transforms.RandomRotation(theta/np.pi*180,interpolation=transforms.InterpolationMode.BILINEAR),
                                        transforms.CenterCrop(1024),transforms.RandomHorizontalFlip(p=0.5),
                                        ])
                self.transform_norm =transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))
            else :
                ... #TODO

        elif resolution ==512:
            if transform:
                self.transform_down =  transforms.Resize(img_size,antialias=True)

                intermediate_size = 600
                theta = np.pi/4 -np.arccos(intermediate_size/(np.sqrt(2)*512)) #Check dataloading.ipynb in analysis-depot for more details

                self.transform_enc =  transforms.Compose([transforms.ToTensor(),
                                        transforms.Resize(intermediate_size,antialias=True),
                                        transforms.RandomRotation(theta/np.pi*180,interpolation=transforms.InterpolationMode.BILINEAR),
                                        transforms.CenterCrop(512),transforms.RandomHorizontalFlip(p=0.5),
                                        ])
                self.transform_norm =transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))
            else:
                ... # TODO

    def __len__(self):
        return len(self.df)

    def __getitem__(self,idx):
        path =  self.df.iloc[idx].Filepath
        img = Image.open(path)
        img_enc = self.transform_enc(img).type(torch.float32)
        img_enc  = self.transform_norm(img_enc)

        return img_enc,torch.empty(())

