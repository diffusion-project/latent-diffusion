# Latent Diffusion

We provide a partial reimplementation of the paper [*High-Resolution Image Synthesis With Latent Diffusion Models*](https://openaccess.thecvf.com/content/CVPR2022/html/Rombach_High-Resolution_Image_Synthesis_With_Latent_Diffusion_Models_CVPR_2022_paper.html) by Rombach et al. The pipeline contains the training and sampling for an image-to-image conditioned LDM model.

## Background
Latent diffusion models (LDMS) are diffusion models which are trained on a latent image space. This latent space is lower dimensional and perceptually equivalent w.r.t. the RGB space, providing various computational benefits. The latent space is defined by a pretrained autoencoder model, usually a [*VQGAN*](https://openaccess.thecvf.com/content/CVPR2021/html/Esser_Taming_Transformers_for_High-Resolution_Image_Synthesis_CVPR_2021_paper.html?ref=https://githubhelp.com). The DM then learns to generate these latent encodings, such that after successful training, the model-generated latent images are revealed with the respective pretrained decoder.

For this lab, we are interested in tackling the task of latent Super-Resolution. Thus, the image generation is conditioned on a smaller sized input image; the model's task is to copy all low-frequency details from the conditioning input and learn to add the remaining high-frequency details to return the upscaled image.

In this pipeline, image encoding was done *on the fly* due to storage limitations.

## Recreating results
We used the following modules: 
* Python/3.10.4
* CUDA/11.8.0
* cuDNN/8.6.0.163-CUDA-11.8.0 

The python libraries are stored in the requirements.txt file and can be installed with: 
```pip install -r requirements.txt```
a virtual environment is recommended. 

### Model training 
To train the model, follow the steps: 
1. Create an experiment folder with the [experiment creator](experiment_creator.ipynb) and the wished settings for training. 
2. within the repository folder, run ```python main.py train <path to experimentfolder>/settings```


### Model sampling
1. Make sure that the checkpoint file is within the **trained_ldm** folder within the experiment folder. Alternatively one can create this folder manually and add the checkpoint file. 
2. Also make sure that the correct checkpoint name is given in the json file ```settings/sample_settings.json```
otherwise the sampling will be done with randomly initialized weights. 
3. within the repository folder, run ```python main.py sample <path to experimentfolder>/settings```


